class Book:
    def __init__(self, author, title, print_year):

        self.author = author
        self.title = title
        self.year = print_year

    def get_author(self):
        return "Author: " + self.author

    def get_title(self):
        return "Title: " + self.title

    def print_year(self):
        return "Printed in: " + self.year

book1 = Book("Natalie", "Hench", "1999")


print(book1.get_author())  # prints "Author: Natalie Zina Walschots"
print(book1.get_title())   # prints "Title: Hench"
print(book1.print_year())
