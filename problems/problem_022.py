# Complete the gear_for_day function which returns a list of
# gear needed for the day given certain criteria.
#   * If the day is not sunny and it is a workday, the list
#     needs to contain "umbrella"
#   * If it is a workday, the list needs to contain "laptop"
#   * If it is not a workday, the list needs to contain
#     "surfboard"

def gear_for_day(is_workday, is_sunny):
    #need if statements to check for peramiters
    if is_workday and is_sunny == False:
        return ["umbrella" + " " + "laptop"]
    elif is_workday:
        return ["laptop"]
    else:
        return ["surfboard"]

is_workday = False
is_sunny = False
print(gear_for_day(is_workday, is_sunny))
# can use append into blank list []
