# Complete the remove_duplicate_letters that takes a string
# parameter "s" and returns a string with all of the
# duplicates removed.
#
# Examples:
#   * For "abc", the result is "abc"
#   * For "abcabc", the result is "abc"
#   * For "abccba", the result is "abc"
#   * For "abccbad", the result is "abcd"
#
# If the list is empty, then return the empty string.

def remove_duplicate_letters(s):
    if s == 0:
        return None
    # need to make a new list
    dup_letters = ""

    for letter in s:
        if letter not in dup_letters:
            dup_letters = dup_letters + letter

    return dup_letters
s = "bananna"
print(remove_duplicate_letters(s))
