# Complete the has_quorum function to test to see if the number
# of the people in the attendees list is greater than or equal
# to 50% of the number of people in the members list.

def has_quorum(attendees_list, members_list):
    attendees = len(attendees_list)
    members = len(members_list)
    if members / attendees >= 0.5:
        return True
    else:
        return False

attendees_list = [1, 5]
members_list = [1]

print(has_quorum(attendees_list, members_list))
