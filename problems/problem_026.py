# Complete the calculate_grade function which accepts
# a list of numerical scores each between 0 and 100.
#
# Based on the average of the scores, the function
# returns
#   * An "A" for an average greater than or equal to 90
#   * A "B" for an average greater than or equal to 80
#     and less than 90
#   * A "C" for an average greater than or equal to 70
#     and less than 80
#   * A "D" for an average greater than or equal to 60
#     and less than 70
#   * An "F" for any other average

def calculate_grade(values):
    if len(values) == 0:
        return None
    #list of if statements
    #need to take avg so value / len(value
    score = 0

    for grade in values:
        score = score + grade

    if score / len(values) >= 90:
        return "A"
    elif score / len(values) >= 80:
        return "B"
    elif score / len(values) >= 70:
        return "C"
    elif score / len(values) >= 60:
        return "D"
    else:
        return "F"

print(calculate_grade([55, 85, 99]))
#can asign len(values) to a variable to clean up/shoerten code
